'use strict';

$(window).on('scroll resize', function() {
	$('body > header').css('top', -($(window).scrollTop()/2));

	var navPos = $(window).height() - $('nav').outerHeight();

	if( $(window).scrollTop() >= navPos ) {
		$('nav').addClass('fixed');
	}
	else {
		$('nav').removeClass('fixed');
	}
});

$(window).on({
	hashchange: function() {
		var hash = window.location.hash.replace('#', '');
		if( hash ) {
			var $section = $('section.' + hash);

			if( $section.length > 0 ) {
				var offsetTop = $section.offset().top - parseInt($('main nav').height());

				$('html, body').animate({ scrollTop: offsetTop }, 500);
			}
		}
	}
});

$(document).ready(function() {
	$('a[rel="navigation"]').click(function() {
		if( window.location.hash == $(this).attr('href') ) {
			$(window).trigger('hashchange');
		}
	});
});

$(document).ready(function() {
	$('section.about article .text .link').click(function(event) {
		event.preventDefault();

		$('body').addClass('modal-showing');
	});

	$('.modal .close').click(function(event) {
		event.preventDefault();

		$('body').removeClass('modal-showing');
		$('.modal').animate({ opacity: 0 }, 200, function() {
			$(this).hide();
		});
	});

	if( typeof(google) != 'undefined' && google.maps ) {
		var map = new google.maps.Map(document.querySelector('section.contact article .gmaps'), {
			zoom: 17,
			center: new google.maps.LatLng(-23.509, -46.628),
			scrollwheel: false,
			navigationControl: false,
			mapTypeControl: false,
			scaleControl: false,
			zoomControl: false,
			panControl: false,
			rotateControl: false,
			streetViewControl: false,
			draggable: false
		});

		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(-23.507645, -46.625543),
			map: map
		});

		google.maps.event.addDomListener(window, 'resize', function() {
			var center = map.getCenter();
			google.maps.event.trigger(map, 'resize');
			map.setCenter(center);
		});
	}

	$('section.contact article .form').submit(function(event) {
		event.preventDefault();

		var stop = false
		var $form = $(this)

		$form.find('input, textarea').each(function() {
			if( $(this).val() == '' ) {
				$(this).closest('.field').addClass('error');
				stop = true;
			}
			else {
				$(this).closest('.field').removeClass('error');
			}
		});

		$form.find('.messages p').hide();

		if( stop ) {
			$form.find('.messages .invalid').show();
		}
		else {
			$form.find('.messages .sending').show();

			$.ajax({
				url: this.action,
				type: this.method,
				dataType: 'json',
				data: $form.serialize()
			})
			.done(function(response) {
				var msgClass = 'error';

				if( response && response.success ) {
					msgClass = 'success';
				}

				$form.find('.messages p').hide().filter('.return').addClass(msgClass).text(response.message || 'json error').show();
			})
			.fail(function(xhr, status, err) {
				$form.find('.messages p').hide().filter('.return').addClass('error').text('Ocorreu um erro na requisição: [' + status + '] ' + err).show();
			});
		}
	});
});
