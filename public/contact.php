<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
	if(
		!empty($_POST['name'])
		&& !empty($_POST['email'])
		&& !empty($_POST['subject'])
		&& !empty($_POST['message'])
	)
	{
		require_once 'class.smtp.php';
		require_once 'class.phpmailer.php';

		$mail = new PHPMailer();
		$mail->CharSet = 'utf-8';
		$mail->IsSMTP();
		$mail->Host = 'mail.andrelucatelli.com.br';
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = 'psicologo@andrelucatelli.com.br';
		$mail->Password = 'andre2015';
		$mail->From = 'psicologo@andrelucatelli.com.br';
		$mail->FromName = 'Administrador';
		$mail->IsHTML();
		$mail->Subject = 'Andre Lucatelli - Contato do Site';
		$mail->Body = $mail->AltBody = <<<STR
		<html>
		<head>
		<meta charset="utf-8" />
		</head>
		<body>
			<font face="Arial">
				<p>Este email foi gerado pelo formulário de contato do site www.andrelucatelli.com.br. Por favor, não responda.</p>
				<p>
					Nome: <strong>{$_POST['name']}</strong><br>
					Email: <strong>{$_POST['email']}</strong><br>
					Assunto: <strong>{$_POST['subject']}</strong><br>
					Mensagem: <strong>{$_POST['message']}</strong>
				</p>
			</font>
		</body>
		</html>
STR;
		$mail->addAddress('andrelucatelli@gmail.com', 'Andre Lucatelli');
		$mail->addBCC('rafael@omochi.com.br', 'Rafael Silva');

		if( $mail->Send() )
		{
			$output = array(
				'success' => true,
				'message' => 'Dados enviados com sucesso. Em breve entraremos em contato.'
			);
		}
		else
		{
			$output = array(
				'error' => true,
				'message' => 'Não foi possível enviar o email, tente novamente mais tarde.',
				'error_mail' => $mail->ErrorInfo
			);
		}
	}
	else {
		$output = array(
			'error' => true,
			'message' => 'Dados insuficientes para enviar o email, preencha o formulário novamente.'
		);
	}
}
else
{
	$output = array(
		'error' => true,
		'message' => 'Método de requisição inválido. Use POST.'
	);
}

header('Content-Type: application/json');
echo json_encode($output);

?>
